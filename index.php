<?php
  require_once('Dado.php');
  $dado = new Dado();
?>
<html>
<h1>Tirada de Dados (12)</h1>
<ul>
<?php for ($i=1; $i <= 12 ; $i++) : ?>
<li> Tirada nº <?=$i?>, resultado: (<?php echo $dado->tirarDado(); ?>)</li>
<?php endfor; ?>
</ul>
</html>
