<?php

class Dado
{
    private int $minNumDado = 0;
    private int $maxNumDado = 12;

    /**
     * Get the value of maxNumDado
     */
    public function getMaxNumDado()
    {
        return $this->maxNumDado;
    }

    /**
     * Set the value of maxNumDado
     *
     * @return  self
     */
    public function setMaxNumDado($maxNumDado)
    {
        $new_number = ($maxNumDado > 12) ? 12 : $maxNumDado;
        $this->maxNumDado = $new_number;

        return $this;
    }

    /**
     * Get the value of minNumDado
     */
    public function getMinNumDado()
    {
        return $this->minNumDado;
    }

    /**
     * Set the value of minNumDado
     *
     * @return  self
     */
    public function setMinNumDado($minNumDado)
    {
        $new_number = ($minNumDado < 0) ? 0 : $minNumDado;
        $this->minNumDado = $new_number;

        return $this;
    }

    public function tirarDado() {
        return rand($this->minNumDado, $this->maxNumDado);
    }
}
